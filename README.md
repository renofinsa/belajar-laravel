## How Install

- git clone https://gitlab.com/renofinsa/belajar-laravel
- go to folder project (cd belajar-laravel)
- composer install
- php artisan key:generate
- copy .env.example file then, config your database in .env file (cp .env.example .env)
- php artisan migrate
- php artisan db:seed
- php artisan serve