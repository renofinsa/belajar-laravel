<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Post;
use App\Models\PostComment;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        
        /**=========================== USER ============================= */
        User::create([
            'name' => 'reno finsa',
            'email' => 'renofinsa@gmail.com',
            'password' => Hash::make('apaaja123')
        ]);

        User::create([
            'name' => 'catur',
            'email' => 'catur@gmail.com',
            'password' => Hash::make('apaaja123')
        ]);
        /**=========================== USER ============================= */

        /**=========================== POST ============================= */
        Post::create([
            'title' => 'Laravel',
            'content' => 'how to make authorization with breeze?',
            'user_id' => 2
        ]);
        Post::create([
            'title' => 'HTML CSS',
            'content' => 'whats is html? css?',
            'user_id' => 2
        ]);
        /**=========================== POST ============================= */

        /**=========================== POST COMMENT ============================= */
        PostComment::create([
            'content' => 'so ez dude ...',
            'post_id' => 1,
            'user_id' => 1
        ]);

        PostComment::create([
            'content' => 'so how? can u tell me?',
            'post_id' => 1,
            'user_id' => 2
        ]);
        /**=========================== POST COMMENT ============================= */
    }
}
