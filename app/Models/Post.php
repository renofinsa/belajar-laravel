<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    public function author() {
        return $this->belongsTo(User::class, 'user_id'); // parent_key
    }

    public function comments() {
        return $this->hasMany(PostComment::class, 'post_id'); // child_key
    }
}
