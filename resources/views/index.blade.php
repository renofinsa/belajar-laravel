<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>POSTs</title>
</head>
<body>
  <div>
    @foreach ($data as $item)
      <div style="border: black solid 1px; margin-bottom: 15px; padding: 5px">
        <h5>{{$item->title}} -> {{$item->author->name}}</h5>
        <p>{{$item->content}}</p>
        <ul>
          @foreach ($item->comments as $comment)
            <li>{{$comment->author->name}} - {{$comment->content}}</li>
          @endforeach
        </ul>
      </div>
    @endforeach
  </div>
</body>
</html>